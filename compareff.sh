#!/bin/bash

# Check if exactly two arguments are given (two directories to compare)
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 directory1 directory2"
    exit 1
fi

# Assign directories to variables for better readability
dir1="$1"
dir2="$2"

# Check if the provided directories exist
if [ ! -d "$dir1" ] || [ ! -d "$dir2" ]; then
    echo "Both arguments must be existing directories."
    exit 1
fi

# File to store the modifications
mods_file="mods.txt"
# Empty the mods.txt file if it already exists, or create it if it doesn't
> "$mods_file"

# Use diff to compare the directories recursively
# -r for recursive, -q for brief (only show files that differ)
diff -rq "$dir1" "$dir2" | while read -r line; do
    # Check if the line indicates a difference
    if [[ "$line" == *"differ"* ]]; then
        # Extract the file path from the diff output
        # This sed command extracts the first path (considering spaces in names)
        file_path=$(echo "$line" | sed -n 's/^Files \(.*\) and .* differ$/\1/p')
        # Append the file path to mods.txt
        echo "$file_path" >> "$mods_file"
    fi
done

echo "Modifications saved to $mods_file."